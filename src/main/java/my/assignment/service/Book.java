package my.assignment.service;

import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class Book {
	
	@PositiveOrZero
	private int id;
	
	@Size(min=2)
	private String description;
	
	public Book(int id, String description) {
		this.id = id;
		this.description = description;  
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", decsciption=" + description + "]";
	}


	
}
