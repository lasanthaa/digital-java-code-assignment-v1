package my.assignment.service;

import java.net.URI;
import java.net.URL;
import java.util.List;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class BookController {
	
	@Autowired
	private BookDaoService bookService;
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("/books")  
	public List<Book> findAll(){
		return bookService.findAll(); 
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/books/{id}")
	public Book findOne(@PathVariable int id){
		
		Book book = bookService.findOne(id);
		
		if(book == null) {
			throw new BookNotFoundException("id: " + id);
		}
		return bookService.findOne(id);
	}
	
	/**
	 * 
	 * @param book
	 * @return
	 */
	@PostMapping("/books")
	public ResponseEntity<Object> createBook(@Valid @RequestBody Book book) {
		Book bookVariable= bookService.save(book);
		
		URI location = ServletUriComponentsBuilder
			.fromCurrentRequest()
			.path("/{id}")
			.buildAndExpand(bookVariable.getId()).toUri();
		
		return ResponseEntity.created(location).build();
		
	}
}
