package my.assignment.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class BookDaoService {
	
	private static List<Book> books = new ArrayList<>();
	
	private static int bookCount = 3; 
	
	static {
		books.add(new Book(1, "Rich-dad-poor-dad"));
		books.add(new Book(2, "Jane Eyre part 1"));
		books.add(new Book(3, "Aice Adventures"));
	}
	
	/**
	 * 
	 * @return List<Book>
	 */
	public List<Book> findAll(){
		return books;
	}
	
	/**
	 * 
	 * @param book
	 * @return
	 */
	public Book save(Book book) {
		
		if(book.getId() == 0 ) {	
			book.setId(++bookCount);
		}
		
		for(Book bookVar:books) {
			if(book.getId() == bookVar.getId()) {
				bookVar.setDescription(book.getDescription());
				return book;
			}
		}

		books.add(book);
	
		return book;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Book findOne(int id){
		for(Book book:books) {
			System.out.println(book.getDescription());
			if(book.getId() == id) {
				return book;
			}
		}
		return null;
	}
	
	
}
