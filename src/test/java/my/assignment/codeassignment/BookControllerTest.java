package my.assignment.codeassignment;


import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import my.assignment.service.Book;
import my.assignment.service.BookDaoService;
import my.assignment.service.CodeAssignmentApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CodeAssignmentApplication.class,webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
public class BookControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private BookDaoService bookService;

	Book mockBook = new Book(1,"Rich-dad-poor-dad");

	String exampleJson = "{\"id\":\"1\",\"description\":\"Rich-dad-poor-dad\"}";
	String exampleJson2 = "{\"id\":\"4\",\"description\":\"test-book\"}";

	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void retrieveDetailsForBook() throws Exception {

		Mockito.when(
				bookService.findOne(1)).thenReturn(mockBook);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"http://localhost:8080/books/1").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse().getContentAsString());
		String expected ="{id:1,description:Rich-dad-poor-dad}";
		
		System.out.println();
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void createTestBook() throws Exception {
		Book mockBook = new Book(4, "test-book");

		Mockito.when(
				bookService.save(Mockito.any(Book.class))).thenReturn(mockBook);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
				"http://localhost:8080/books")
				.accept(MediaType.APPLICATION_JSON).content(exampleJson2)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();
		
		assertEquals(HttpStatus.CREATED.value(), response.getStatus());

		assertEquals("http://localhost:8080/books/4", response
				.getHeader(HttpHeaders.LOCATION));

	}


}
