# README

## Steps and commands to run the application

1. Import the project into eclipse ide. (open -> open project from file system)
2. Set the goal as spring-boot:run in project->run as->run-configurartion->maven-build and change the workspace to current project directory.
3. Then run the project

## API testing commands
```javascript
1.GET /books
    curl -X GET http://localhost:8080/books 
```    
```javascript
Payload :
[ {   "id": 1,  "description": "Rid dad poor dad" },
  { "id": 2,  "description": "Jane Eyre part 1" },
  {"id": 3, "description": "Aice Adventures"  }
]
```
```javascript
2.GET /books/1
    curl -X GET http://localhost:8080/books/1
 ```   
 ```javascript
Payload :
{  "id": 1 , "description": "Rid dad poor dad"}
```

```javascript
3.POST /books
    curl -X POST http://localhost:8080/books -H 'content-type: application/json  -d '{	"description" : "test book" }
  ```
